pyyaml>=5.3.1
setuptools>=65.6.3
toml>=0.10.2
jupyterlab >= 3.5.2
matplotlib >= 3.6.2
ipywidgets >= 8
sphinxcontrib-bibtex
sphinx-copybutton
scipy
numpy
ipywidgets
ipympl
chardet
pandas
black
pre-commit
sphinx
pandoc
myst-parser
nbsphinx
sphinx_rtd_theme
myst_nb
