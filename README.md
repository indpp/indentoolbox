# Indentation Toolbox

Smart indentation testing post-processing with Python.

## Installation

Before installing IndenToolBox, you'll need the following tools:
* A GIT client.
* A Python environment manager (e.g. conda).

### For standard users

* Create a blank Python environment. From example, with `conda`:

```
conda create -y -n indentation pip
conda activate indentation
```

* Install IndenToolBox with the following command:

```
pip install git+https://gitlab.com/indpp/indentoolbox.git@dev
```

### For contributors

* Fork the repo from GITLab.
* Choose your working directory.
* Clone the repo from your fork in this directory:

```
git clone https://gitlab.com/<user>/indentoolbox.git
```

Where `<user>` is your GITLab username.

* Checkout to dev branch.

```
cd indentoolbox
git checkout dev
```

Or create a new branch.

```
cd indentoolbox
git checkout -b mybranch dev
```

* Make sure you're in a clean Python environment:

```
conda create -y -n indentation pip
conda activate indentation
```

* And then install IndenToolBox in editable mode:

```
pip install -e .
pip install -r requirements.txt
```

Then you can work and, if you wish, commit your work, push it and finally create a pull request on GITLab to share your work with the main repository.

## Documentation

[Documentation on ReadTheDocs](https://indentoolbox.readthedocs.io/en/latest/)
