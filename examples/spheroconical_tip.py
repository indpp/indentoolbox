import indentoolbox as itb
import matplotlib.pyplot as plt
import numpy as np

psi = 70.29

# R = 200.e-6
htrunc = 15.0e-9

tip = itb.core.SpheroConicalTip(angle=psi, htrunc=htrunc)
R = tip.radius

h = np.linspace(0.0, R, 1000)
rc = tip.contact_radius(h)
rc_sphere = np.sqrt(h * (2 * R - h))
htrunc = R * (1.0 / np.sin(np.radians(psi)) - 1.0)
Ac = tip.contact_area(h)
Ac_sphere = np.pi * rc_sphere**2
fig = plt.figure()
ax = fig.add_subplot(2, 1, 1)
# ax.set_aspect("equal")
plt.plot(h * 1.0e9, rc * 1.0e9, "r-", label="Sphero-cone")
plt.plot(h * 1.0e9, rc_sphere * 1.0e9, "--b", label="Sphere")
plt.legend(loc="best")
plt.grid()
# plt.xlabel("Contact heigth, $h$ [nm]")
plt.ylabel("Contact radius, $r_c$ [nm]")
plt.savefig("spheroconical_tip.png")
ax = fig.add_subplot(2, 1, 2)
# ax.set_aspect("equal")
plt.plot(h * 1.0e9, Ac * 1.0e12, "r-")
plt.plot(h * 1.0e9, Ac_sphere * 1.0e12, "b--")
plt.grid()
plt.xlabel("Contact heigth, $h$ [nm]")
plt.ylabel("Contact area, $r_c$ [µm$^2$]")
plt.savefig("spheroconical_tip.png")


plt.close("all")
