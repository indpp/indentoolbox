import toml
import pandas as pd
import numpy as np
import indentoolbox as itb
from indentoolbox.core import Test, Batch, Device, Operator, Sample
import toml


def read_hysitron_nano(path, protocol=None, **kwargs):
    """
    Reads a test txt output in the Hysitron nano format.
    """
    lines = open(path).readlines()
    disp, force, time = [[]], [[]], [[]]
    # for pos in [0, 0, 0, 0, 1]:
    #     lines.pop(pos)
    while True:
        line = lines[0]
        words = line.split()
        if len(words != 0):
            if words[0].isnumeric():
                break
        else:
            lines.pop(0)
    for i in range(len(lines)):
        line = lines[i]
        if line.split() == []:
            disp.append([])
            force.append([])
            time.append([])
        else:
            words = line.split()
            disp[-1].append(float(words[0]) * 1.0e-9)
            force[-1].append(float(words[1]) * 1.0e-6)
            time[-1].append(float(words[2]))
    N_steps = len(time)
    if protocol != None:
        if len(protocol) != N_steps:
            print(
                f"<Warning: protocol length {len(protocol)} does not match step number {N_steps}>"
            )
    else:
        protocol = ["Step" for i in range(N_steps)]
    steps = []
    for i in range(N_steps):
        step_kind = protocol[i]
        step_data = pd.DataFrame()
        step_data["time"] = time[i]
        step_data["disp"] = disp[i]
        step_data["force"] = force[i]
        # step = step_mapping[step_kind](data=step_data)
        step_class = getattr(itb.core, step_kind)
        step = step_class(data=step_data)
        steps.append(step)
    test = Test(steps=steps, **kwargs)
    return test


def read_batch_from_folder(folder_path):
    metadata = toml.load(f"{folder_path}/metadata.toml")
    format = metadata["format"]
    file_format = format["file_format"]
    protocol = format["protocol"]

    data = {}
    tests = []
    tests_metadata = metadata["tests"]
    for test_metadata in tests_metadata:
        path = f"{folder_path}/{test_metadata['path_to_data']}"
        # if file_format == "hysitron nano":
        #     test = read_hysitron_nano(path, protocol=protocol, metadata=test_metadata)
        #     tests.append(test)
        test = Test.from_txt(path, protocol, file_format)
        tests.append(test)
    data["tests"] = tests
    data["device"] = Device(**metadata["device"])
    data["operators"] = [Operator(**md) for md in metadata["operators"]]
    data["sample"] = Sample(**metadata["sample"])
    tip_class_name = metadata["tip"]["class"]
    tip_class = getattr(itb.core, tip_class_name)
    data["tip"] = tip_class(**metadata["tip"]["attributes"])
    batch = Batch(**data)
    return batch
    """file_format = metadata["file_format"]

    tests = []
    protocol = metadata["protocol"]
    tests_metadata = metadata["tests"]
    for test_metadata in tests_metadata:
        path = f"{folder_path}/{test_metadata['path_to_data']}"
        if file_format == "hysitron nano":
            test = read_hysitron_nano(path, protocol=protocol, metadata=test_metadata)
            tests.append(test)
    tip_class_name = metadata["tip"]["class"]
    tip_class = getattr(itb.core, tip_class_name)
    tip = tip_class(**metadata["tip"]["attributes"])
    print(tip_class)
    batch = Batch(tests=tests, metadata=metadata, tip=tip)
    return batch, metadata"""
