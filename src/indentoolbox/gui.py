# INDENTOOLBOX / GUI
import ipywidgets as widgets
import indentoolbox as itb
import chardet
import datetime


def file_processing_widgets(setup, protocol, file_format=".txt"):
    """
    Test file selection widget.
    """
    # FILES
    files_upload = widgets.FileUpload(
        accept=".txt", multiple=True, description="Select test files"
    )
    # FILE FORMAT
    format_options = ["hysitron nano", "CSM"]
    file_format = widgets.Dropdown(
        options=format_options,
        value=format_options[0],
        description="Format:",
        disabled=False,
    )

    encoding_options = ["auto", "UTF-8", "UTF-16", "UTF-32", "ISO-8859-1", "ASCII"]
    file_encoding_widget = widgets.Dropdown(
        options=encoding_options,
        value=encoding_options[0],
        description="Encoding:",
        disabled=False,
    )
    # PROCESS
    process = widgets.Button(
        description="Read Tests",
        disabled=False,
        button_style="success",
        tooltip="Click me",
        icon="folder open",
    )

    # OUTPUT
    output = widgets.Output()

    # PROGRESS BAR:

    widget_dict = {
        "files_upload": files_upload,
        "file_format": file_format,
        "process": process,
        "output": output,
    }
    layout = widgets.VBox(
        [
            widgets.HBox(
                [
                    widgets.VBox([file_format, file_encoding_widget]),
                    widgets.VBox([files_upload, process]),
                ]
            ),
            output,
        ]
    )

    def process_files(setup, widget_dict):
        file_format = widget_dict["file_format"].value
        tests = []
        file_encoding = file_encoding_widget.value
        output = widget_dict["output"]
        output.clear_output(wait=False)
        if file_encoding == "auto":
            file_data = files_upload.value[0]
            file_content = file_data["content"].tobytes()
            encoding = chardet.detect(file_content)["encoding"]
            with output:
                display(f"Using detected encoding {encoding}")
        else:
            encoding = file_encoding
            with output:
                display(f"Using selected encoding {encoding}")
        for file_data in files_upload.value:
            with output:
                file_name = file_data["name"]
                display(f"Reading: {file_name}")
            file_content = file_data["content"].tobytes()
            file_content = file_content.decode(encoding)
            test = itb.core.Test.from_txt(
                content=file_content, protocol=protocol, file_format=file_format
            )
            tests.append(test)
        setup["tests"] = tests

    process.on_click(lambda btn: process_files(setup, widget_dict))
    return layout


def date_selection_widget(setup):
    """
    Date selection widget.
    """
    date = widgets.DatePicker(
        value=datetime.date.today(), description="Test date", disabled=False
    )
    setup["date"] = date.value

    def update(btn):
        setup["date"] = date.value

    date.observe(update)
    return date
